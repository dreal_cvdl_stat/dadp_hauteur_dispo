library(shiny)
library(htmlwidgets)
library(webshot)
library(dygraphs)
library(dplyr)
library(shinydashboard)
library(plotly)
library(webshot)
library(tidyr)
library(stringr)
library(DT)

options(encoding = "UTF-8")

source('fonctions_utiles.R', local = TRUE)


source('ui.R', local = TRUE)
source('server.R', local = TRUE)
shinyApp( ui=ui, server=server)
