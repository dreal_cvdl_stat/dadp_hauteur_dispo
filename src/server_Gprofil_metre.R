# graphique interactif onglet Résultats/graphique/en metre
output$Gprofil_metre <- renderDygraph({
  
  profil_2<-Prof()
  Plot_profil<-profil_2 %>% 
    select("X","MNE","MNT",starts_with("Hauteur_metre_dispo_Obs")) %>% 
    rename(Distance=X) %>% 
    rename_all(funs(stringr::str_replace_all(., 'Hauteur_metre_dispo_', '') %>% 
                      stringr::str_replace_all(., '_', '')
    ))
  dy<-dygraph(Plot_profil ,
              xlab="Distance",main=file_name())  %>%
    dyOptions(colors = c(RColorBrewer::brewer.pal(5, "Set2")))%>%
    dyAxis("y", label = "Hauteur disponible en metre") %>%
    dyAxis("y2", label = "MNT/MNE en mNGF") %>% 
    dySeries("MNE", axis = 'y2',color="#008B00")%>% 
    dySeries("MNT", axis = 'y2',color="#8B3A3A")%>% 
    dyRangeSelector
  
  #saveWidget(dy, "temp.html", selfcontained = FALSE)
  #webshot("temp.html", file = "Rplot.png",cliprect = "viewport")
  return(dy)
  
})