#Partie lecture du fichier de donnees
file_name <- reactive({
  inFile <- input$file1
  
  if (is.null(inFile))
    return(NULL)
  
  return (stringi::stri_extract_first(str = inFile$name, regex = ".*(?=\\.)"))
})


# lecture de la table de données  
filedata <- reactive({
  infile <- input$file1
  if (is.null(infile)) {
    # User has not uploaded a file yet
    return(NULL)
  }
  read.csv(infile$datapath,header=TRUE, sep="\t")
  
})